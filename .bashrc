#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '
export PATH="$HOME/.local/bin:$PATH"
complete -cf doas

# Aliases
alias ls='ls --color=auto'
alias git_dotfiles='git --git-dir=$HOME/.git_dotfiles/ --work-tree=$HOME'
alias xfreerdp-win='xfreerdp  /v:192.168.122.6 /u:User /dynamic-resolution -grab-mouse -grab-keyboard'
